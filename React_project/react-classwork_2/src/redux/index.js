import {createStore} from "redux";

const startState = [
        {
            id: 1,
            title: "SOME TITLE1",
            text: 'some text1'
        },
        {
            id: 2,
            title: "SOME TITLE2",
            text: 'some text2'
        },
        {
            id: 3,
            title: "SOME TITLE3",
            text: 'some text3'
        }
    ];

function reducer(state = startState, action) {
    const {type, payload} = action;
    switch (type) {
        default:
            return state
    }
}

const store = createStore(reducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default store;