import React from "react";
import {NavLink} from "react-router-dom";
import './style.sass'


export const Header = props => {
    return (
        <header>
            <nav>
                <NavLink to='/' exact>Home</NavLink>
                <NavLink to='/notes'>Notes</NavLink>
                <NavLink to='/about'>About page</NavLink>
            </nav>
        </header>
    )
};

