import React from 'react';
import './App.css';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import {HomePage, About, Notes} from './components';
import {Header} from './commons'
import {Provider} from 'react-redux'
import store from './redux';



function App() {
  return (
      <Provider store={store}>
          <Router>
              <div className="App">
                  <Header/>
                  <Switch>
                      <Route path='/' exact component={HomePage}/>
                      <Route path='/about' component={About}/>
                      <Route path='/notes' component={Notes}/>
                  </Switch>
              </div>
          </Router>
      </Provider>
  );
}

export default App;
