import React from 'react';

export function ElemList(props){
const arr =  props.arr;
const list = arr.map((item, i) =>  <li key={i}>{item}</li>)
  return(
      <ul>
    {list}
    </ul>
            )
}