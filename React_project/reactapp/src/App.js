import React from 'react';
import {ElemList} from "./components/ElemList";

function App() {
  const  state = {
    arr: ['qw', 'er', 'ert', 4564, 'true']
    }

  return (
    <div className="App">
      <ElemList arr={state.arr}/>
    </div>
  );
}

export default App;
