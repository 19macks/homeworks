import React from 'react';
import { ROuter, Switch, Route, NavLink} from "react-router-dom";
import {HomePage, Heroes} from './components';
import './App.css';

function App() {
  return (
    <div className="App">
      <HomePage/>
    </div>
  );
}
export default App;
