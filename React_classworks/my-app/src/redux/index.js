import {createStore} from 'redux'

const startState = {
    notes : [{
        id: 1,
        title: "zago",
        text: "t"
    },
    {
        id: 2,
        title: "lov",
        text: "ex"
    },
    {
        id: 3,
        title: "ok",
        text: "t"
    }]
}

function reducer(state = startState, action){
    const {type, payload} = action;
    switch(type){
        default :
            return state
    }
}

const store = createStore(
    reducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    )

export default store 