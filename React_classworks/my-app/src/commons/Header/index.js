import React from 'react'
import './style.sass'
import {NavLink} from 'react-router-dom'


export const Header = props => {
    return (
        <header>
            <nav>
                <NavLink to='/' exact>Home</NavLink>
                <NavLink to='/notes'>Notes page</NavLink>
                <NavLink to='/about'>About page</NavLink>
            </nav>
        </header>
        )
}
