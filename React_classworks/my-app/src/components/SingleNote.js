import React from 'react'

export const SingleNote = ({title, text}) => {
    return (
        <div style={{
            padding: "15px",
            border: "1px solid #000"
        }}>
            <h2>{title}</h2>
            <p>{text}</p>
        </div>
    )
}