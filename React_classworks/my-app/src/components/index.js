export {HomePage} from './HomePage';
export {About} from './About';
export {NotesList} from './NotesList';
export {NotePage} from './NotePage';
export {SingleNote} from './SingleNote';