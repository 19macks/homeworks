import React from 'react'
import {connect} from 'react-redux'
import {SingleNote} from './SingleNote'

const mapStateToProps = state => {
    return {
        notes: state.notes
    }
};

export const NotePage = connect(mapStateToProps)(({notes, match, history}) => {
    const {title, text} = notes[match.params.index];
    return (
        <SingleNote title={title} text={text}/>
    )
})