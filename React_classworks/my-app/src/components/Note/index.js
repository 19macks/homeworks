import React, {useState, useRef, useEffect} from 'react';
import './style.sass'

export const Note = props => {
    const [editStatus, setEditStatus] = useState(false);
    const [title, setTitle] = useState("");
    const [text, setText] = useState("");
    const formElement = useRef();


    const editBtnHandler = () => {
        setEditStatus(true);
    };

    const onSubmitHandler = (event) => {
        event.preventDefault();
        console.log(formElement.current[0].value);
        // setEditStatus(false)
    };



    useEffect(() => {
        if (editStatus) {
            titleInput.current.focus()
        }
        console.log('update');
    });

    useEffect(() => {
        console.log('mounted');
    }, []);
    return (
        <div>
            {!editStatus ?
            <div>
                <h1>{title}</h1>
                <p>{text}</p>
                <button onClick={editBtnHandler}>Edit</button>
            </div>
            :
            <div>
                <form ref={formElement} onSubmit={onSubmitHandler}>
                    <input/>
                    <textarea></textarea>
                    <button type='submit'>Save</button>
                </form>
            </div>
            }
        </div>
    )
};