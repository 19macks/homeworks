import React from "react";
import './style.sass'

export class Counter extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            status: false,
            startValue: this.props.startValue
        }
    }
    increaseCounter = () => {
        this.setState((state) => {
            return {
                ...state,
                startValue: state.startValue + 1
            }
        })
    };

    componentDidMount() {
        console.log('mounted');
    }

    componentDidUpdate() {
        console.log('updated');
    }

    componentWillUnmount() {
        console.log('unmount');
    }

    render () {
        return (
            <div className="counter">
                <div className="counterValue">
                    {this.state.startValue}
                </div>
                <button onClick={this.increaseCounter}>+</button>
            </div>
        )
    }
}