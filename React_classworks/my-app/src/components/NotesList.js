import React from "react"
import {SingleNote} from './SingleNote'
import {Link} from 'react-router-dom'
import {connect} from 'react-redux'

const mapStateToProps = state => {
    return {
        notes: state.notes
    }
};

export const NotesList = connect(mapStateToProps)(({notes}) => {
    const List = notes.map((item, index)=>{
        const pathname = "/notes/" + index
        return  (<Link to={pathname}  key={item.id}>
                    <SingleNote title={item.title} text={item.text}/>
                </Link>)
    })
    return(
        <div>
            {List}
        </div>
    )
})