import React, {useState} from 'react';
import {Provider} from 'react-redux'
import './App.css';
import {SimpleForm} from './components/SimpleForm'
import store from './store'
import {ReduxForm} from "./components/ReduxForm";
import {Square} from "./components/Square";

function App() {
  const defaultValues = () => {
    return {
      color: "rgb (255, 255, 255)",
      size: 100,
      padding: 10,
      borderRadius: 0
    }
  }

  const [state, setState] = useState({
    email: "",
    message:""
  });

  const onSubmitHandler = (event,formState) => {
    event.preventDefault();
    setState(formState);
  };

  return (
      <Provider store={store}>
        <div className="App">
           {/*<SimpleForm onSubmit = {onSubmitHandler}/>*/}
           {/*------------------------------------*/}
           {/* <h1>{state.email}</h1>*/}
           {/* <h2>{state.message}</h2>*/}

           <ReduxForm defaultValues={defaultValues()}/>
           <Square/>
        </div>
      </Provider>
  );
}

export default App;
