import React, {useState} from 'react';

export const SimpleForm = (props) => {

    const [state, setState] = useState({
        email: "",
        message: "",
        color: "",
        width: "",
        height: "",
    });

    const onChangeHandler = (event) => {
        const name = event.target.getAttribute("name");
        const value = event.target.value;
            setState({
                ...state,
                [name]: value
            });
        console.log(state);
    };

    return (
        <div>
            <form onSubmit={(event) => props.onSubmit(event, state)}>
                <div>
                    <input type='text' name="email" value={state.email} onChange={onChangeHandler}/>
                </div>
                <div>
                    <textarea type="message" name="message" value={state.message} onChange={onChangeHandler}/>
                </div>

                <div>
                    <input type='color' name="color" value={state.color} onChange={onChangeHandler}/>
                </div>
                <div>
                    <input type='text' name="width" value={state.width} onChange={onChangeHandler}/>
                </div>
                <div>
                    <input type='text' name="height" value={state.height} onChange={onChangeHandler}/>
                </div>



                <button type="submit">Submit</button>
            </form>
            <div style={{
                backgroundColor: state.color,
                width: +state.width,
                height: +state.height
            }}/>
        </div>
         )
};

