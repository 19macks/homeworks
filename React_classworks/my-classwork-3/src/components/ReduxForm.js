import React from "react"
import {Field, reduxForm} from "redux-form"

export const ReduxForm = reduxForm({
    form: "styles"
}) (props => {
    return(
        <form onSubmit={e=>e.preventDefault()}>
            <div>
                <Field
                    name = "padding"
                    component="input"
                    type="number"
                />
            </div>
            <div>
                <Field
                    name = "color"
                    component="input"
                    type="color"
                />
            </div>
            <div>
                <Field
                    name = "size"
                    component="input"
                    type="number"
                />
            </div>
            <div>
                <Field
                    name = "borderRadius"
                    component="input"
                    type="number"
                />
            </div>
            <button type="submit">Submit</button>
        </form>
    )
})