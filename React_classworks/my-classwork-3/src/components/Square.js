import React from 'react'
import {connect} from "react-redux"

const mapStateToProps = (state) => {
    return ({
        ...state
    })
}

export const Square  = connect(mapStateToProps)( props => {
    let styles = null;
    if (props.form.styles) {
        styles = props.form.styles.values
    }
    return(
        <div>
            {styles ?
                <div style={{
                    padding: +styles.padding,
                    border: "2px solid black",
                    transition: "all 0.5s ease"
                }}>
                    <div style={{
                        backgroundColor: styles.color,
                        height: +styles.size,
                        width: +styles.size,
                        borderRadius: +styles.borderRadius,
                        margin: "10px auto",
                        transition: "all 1s ease"
                    }}/>
                </div>
                : null
            }
            {/* --------------------------------- */}
        </div>
    )
});
