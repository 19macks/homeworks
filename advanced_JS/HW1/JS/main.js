
function Hamburger(size, stuffing) {
    this.price = size.price + stuffing.price;
    this.calories = size.calories + stuffing.calories;
}

Hamburger.prototype.addTopping = function (topping) {
    this.price += topping.price;
    this.calories += topping.calories;
};

Hamburger.SIZE_SMALL = {price: 50, calories: 20};
Hamburger.SIZE_LARGE = {price: 100, calories: 40};
Hamburger.STUFFING_CHEESE = {price: 10, calories: 20};
Hamburger.STUFFING_SALAD = {price: 20, calories: 5};
Hamburger.STUFFING_POTATO = {price: 15, calories: 10};
Hamburger.TOPPING_MAYO = {price: 20, calories: 5};
Hamburger.TOPPING_SPICE = {price: 15, calories: 0};



let burger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
burger.addTopping(Hamburger.TOPPING_MAYO);
console.log(burger);
