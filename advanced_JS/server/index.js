const express = require('express');
const fs = require('fs');
const ejs = require('ejs');
const port = 3000;

const app = express();

app.set('view engine', 'ejs');

app.get('/', async (req, res) => {

    let request = await fetch('https://swapi.co/api/starships');

    let ships = await request.json()

    res.render('index', {name: req.query.name});
});

app.listen(port, () => {
    console.log('Server started at port 3000');
});

