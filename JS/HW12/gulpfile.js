const gulp = require('gulp'),
    sass = require('gulp-sass'),
    clean = require('gulp-clean'),
    babel = require('gulp-babel'),
    browserSync = require('browser-sync').create(),
    image = require('gulp-image'),
    autoprefixer = require('gulp-autoprefixer');

const path = {
    build: {
        html: 'build',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/img/'
    },
    src: {
        html: 'src/index.html',
        js: 'src/js/main.js',
        style: 'src/scss/**/*.scss',
        img: 'src/img/**/*.*'
    },
    watch: {
        html: 'src/index.html',
        js: 'src/js/main.js',
        style: 'src/scss/**/*.scss',
        img: 'src/img/**/*.*'
    },
    clean: './build/'
};

/*************CALLBACK*************/
const htmlBuild = () => {
    return gulp.src(path.src.html).pipe(gulp.dest(path.build.html, {allowEmpty: true}));
};

const scssBuild = () => {
    return gulp.src(path.src.style)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            overrideBrowsersList: ['>0.2%'],
            cascade: false
        }))
        .pipe(gulp.dest(path.build.css));
};

const jsBuild = () => {
    return gulp.src(path.src.js)
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(gulp.dest(path.build.js))
};

const imgBuild = () => {
    return gulp.src(path.src.img)
        .pipe(image({
            pngquant: true,
            optipng: false,
            zopflipng: true,
            jpegRecompress: false,
            mozjpeg: true,
            guetzli: false,
            gifsicle: true,
            svgo: true,
            concurrent: 10,
            quiet: true // defaults to false
        }))
        .pipe(gulp.dest(path.build.img));
};

const watcher = () => {
    browserSync.init({
        server: {
            baseDir: "./build"
        }
    });
    gulp.watch(path.watch.html, htmlBuild).on('change', browserSync.reload);
    gulp.watch(path.watch.style, scssBuild).on('change', browserSync.reload);
    gulp.watch(path.watch.js, jsBuild).on('change', browserSync.reload);
    gulp.watch(path.watch.img, imgBuild).on('change', browserSync.reload);
};

const cleanBuild = () => {
    return  gulp.src(path.clean,{allowEmpty: true}).pipe(clean());
};

/*************TASK*************/
gulp.task('htmlBuild', htmlBuild);
gulp.task('clean', cleanBuild);
gulp.task('scssBuild', scssBuild);
gulp.task('jsBuild', jsBuild);
gulp.task('build', gulp.series(
    cleanBuild,
    htmlBuild,
    scssBuild,
    jsBuild,
    imgBuild,
    watcher
));