let filterBy = (arr, typeOfArr) => {
    let newArr = [];
    // return newArr;
    arr.forEach(function (elem) {
        if (typeof elem !== typeOfArr) {
            newArr.push(elem);
        }
    });
    return newArr;
};


console.log(filterBy([14, null, 23, "I", true, "Love", undefined, {name: "Leo"}, false, "JavaScript", 25], "number"));
