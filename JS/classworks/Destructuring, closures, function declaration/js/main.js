class Machine {
    constructor() {
        this._enabled =  false;
        this._electricity = false;
    }
    toggle(value){
        if (value !== this._enabled) {
            if (!this._enabled && this._electricity) {
                this._enabled = true;
                console.log("machine is enabled!");
            } else  {
                this._enabled = false;
                console.log("machine is disabled!");
            }
        }
    }
    set electricity(value){
        this._electricity = value;
        if (!value) this.toggle(value);
    }
}

let obj = new Machine;


obj.electricity = true;
obj.toggle();
obj.electricity = false;
obj.toggle();
obj.electricity = true;
obj.toggle(false);

class CoffeMachine extends Machine {
    constructor(power){
        super();
        this._running = false;
        this._power = power;
    }
    showPower(){
        // console.log(this._power);
    }
    makeCoffe(coffeType){
        if (this._enabled && !this._running) {
            console.log(`${coffeType} in progress`);
            this._running = true;
            setTimeout(() => {
                this._running = false;
                console.log(`${coffeType} is ready!`);
            }, 2000);
        } else {
            console.log("wait a min");
        }
    }
    toggle(value) {
        if (this._running) {
            console.log("coffe machine is stopped");
        }
        super.toggle(value)
    }
}

let samsung = new CoffeMachine(1);

samsung.showPower();

samsung.electricity = true;

samsung.toggle();

samsung.makeCoffe("late");
// samsung.makeCoffe("late");
//
// setTimeout(() => {samsung.makeCoffe("capuchino")}, 5000);
setTimeout(() => {samsung.makeCoffe("capuchino")}, 1000);