
function arrFromCollection (collection) {
    return Array.from(collection);
}

let tabsContent = document.getElementsByClassName("tabs-content");
let allTabs = document.getElementsByClassName("tabs-title");

let arrTabsContent = arrFromCollection(tabsContent);
let arrAllTabs = arrFromCollection(allTabs);


document.getElementById("all-tabs").onclick = function (event) {
    arrAllTabs.forEach(function (item) {
        item.classList.remove("active");
    });
    event.target.classList.add("active");
    let tabsAttribute = event.target.getAttribute("data-name");
    arrTabsContent.forEach(function (elem) {
        let contentAttribute = elem.getAttribute("data-name");
        console.log(contentAttribute);
        if (tabsAttribute === contentAttribute) {
            elem.classList.add("active");
        } else {
            elem.classList.remove("active");
        }
    })
};