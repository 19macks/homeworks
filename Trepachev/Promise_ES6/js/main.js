let time = function (min, max) {
    // случайное число от min до (max+1)
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand * 1000);
};

let arr = [
    new Promise(function (resolve, reject) {
        let time1 = time(1,4);
        setTimeout(function () {
            console.log(time1);
            resolve(1);
        }, time1)
    }),
    new Promise(function (resolve, reject) {
        let time2 = time(1,4);
        setTimeout(function () {
            console.log(time2);
            resolve(2);
        }, time2)
    }),
    new Promise(function (resolve, reject) {
        let time3 = time(1,4);
        setTimeout(function () {
            console.log(time3);
            resolve(3);
        }, time3)
    })
];


Promise.race(arr).then(value => {
    alert(value);
});

// function heavyOperation(num) {
//     return new Promise(function (resolve, reject) {
//         setTimeout(() => {
//             if (num !== 0) {
//                 resolve (num * num);
//             } else {
//                 reject ('error')
//             }
//         }, 1000)
//     })
// }
//
// let num = [1, 0, 3];


