function square(num) {
    return (num * num);
}

function cube (num) {
    return (num * num * num);
}

function power (num, times) {
    let result = num;
    for (let i = 1; i < times; i++) {
        result = result * num;
    }
    return result;
}

exports.square = square;
exports.cube = cube;
exports.power = power;