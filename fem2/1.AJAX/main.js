// let obj = {
//     name: "Maks",
//     pass: "somePass",
// };
//
// let objJSON = JSON.stringify(obj);
// console.log(objJSON);
//
// let parsedData = JSON.parse(objJSON);
//
// console.log(parsedData.name);

// --------------------------------------------------------------------------------------
//
// const content = document.getElementById("content");
//
// let request = new XMLHttpRequest();
//
// request.onreadystatechange = () => {
//     if (request.readyState === 4) {
//         let response = request.response;
//         let objJSON = JSON.parse(response);
//         console.log(objJSON);
//         if (objJSON.results) {
//             renderList(objJSON.results);
//         } else {
//             renderItemPage(objJSON)
//         }
//     }
// };
//
// request.open("GET", "https://swapi.co/api/people/", true);
//
// request.send();
//
// function renderList(list) {
//     const container = document.createElement("div");
//   list.forEach(el => {
//       const listItem = document.createElement("h3");
//       listItem.className = "person";
//       listItem.dataset.url = el.url;
//       listItem.innerText =  el.name;
//       container.appendChild(listItem);
//   });
//     content.innerHTML = "";
//     content.appendChild(container)
// }
//
// content.addEventListener("click", function (e) {
//     if (e.target.classList.contains("person")) {
//         request.open('Get', e.target.dataset.url, true);
//         request.send();
//     }
// });
//
// function renderItemPage(el) {
//     const container = document.createElement("div");
//     const inner = `
//         <h4>Name: ${el.name}<h4/>
//         <p>Birthday: ${el.birth_year}<p/>
//         <p>Hair Color: ${el.hair_color}</p>
//         <p>Skin Color: ${el.skin_color}</p>
//     `;
//     container.innerHTML = inner;
//     content.innerHTML = "";
//     content.appendChild(container);
// }
//
// ------------------------------------------------------------------------------------------

// let result;
// let promise = new Promise((resolve) => {
//     setTimeout(() => {
//         resolve("request done!");
//     }, 3000)
// });
//
// promise.then(
//     (val) => {
//         result = val;
//         console.log(result);
//     }
// );

//--------------------------------------------------------------------------------------------

// const response = fetch("https://swapi.co/api/people/1");
//
// response.then((result) => {
//     const data = result.json();
//     return data;
// }).then(parsedData => {
//     console.log(parsedData);
// });

//---------------------------------------------------------------------------------------------

async function fetchData() {
    const result = await fetch("https://swapi.co/api/people/1");
    console.log(result);
    const data = await result.json();
    console.log(data);
}

fetchData();