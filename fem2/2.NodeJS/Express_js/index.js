// ! create index.js
// npm init -y
// npm i express nodemon

const db = [{
    "id": 1,
    "first_name": "Madlen",
    "last_name": "Kingwell"
  }, {
    "id": 2,
    "first_name": "Coraline",
    "last_name": "Vidloc"
  }, {
    "id": 3,
    "first_name": "Orren",
    "last_name": "Mablestone"
  }, {
    "id": 4,
    "first_name": "Tammara",
    "last_name": "Lowman"
  }, {
    "id": 5,
    "first_name": "Beverly",
    "last_name": "Sunner"
  }, {
    "id": 6,
    "first_name": "Fielding",
    "last_name": "Daens"
  }, {
    "id": 7,
    "first_name": "Killian",
    "last_name": "Atkinson"
  }, {
    "id": 8,
    "first_name": "Vittoria",
    "last_name": "Gergely"
  }, {
    "id": 9,
    "first_name": "Beltran",
    "last_name": "Cosbee"
  }, {
    "id": 10,
    "first_name": "Quentin",
    "last_name": "Woollaston"
  }]

const express = require("express");
const app = express();

app.set("view engine", "ejs");
app.use(express.static('public'));


app.get("/", (req, res) => {
    const data = db;
    
    res.render("homepage", {data});
});

app.get("/about", (req, res)=> {
    console.log("it's about");
    res.send("about your history");
})


app.post("/items", (req, res)=> {
    console.log("it's items");
    res.json(JSON.stringify({items:[1,2,3,4,5]}));
})


app.listen(3000, () => {
    console.log("Starting to listen to port 3000!");
})

// ! in Browser console write it down:
// fetch("/items", {method: "POST"})
// .then((res) => res.json())
// .then((json) => console.log(JSON.parse(json)));