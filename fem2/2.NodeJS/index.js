var http = require("http");
const hostname = '127.0.0.1';
const port = 8000;

const server = http.createServer();

server.on('request', (request, res) => {
    console.log(request);
    res.statusCode = 301;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello World!\n');
});


server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});