var express = require('express');
var app = express();

app.get('/', function (req, res) {
    console.log('I want /')    
    res.send('Hello World!');
});

app.get('/item', function (req, res) {
    console.log('I want /test')
    res.json({
        items: [
            1,2,3,4,5
        ]
    });
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});
