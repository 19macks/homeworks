const fs = require("fs").promises;

const objFile = {
    name: "John",
    surname: "Dir",
    age: 30
};

// fs.appendFile("some.json", JSON.stringify(objFile), "utf8", (err) => {
//     console.log(err)
//     console.log("Recording is successfully executed!");
// })

const textData = `name: Bob,
                  surname: Green`;

const promiseFileAppend = fs.appendFile("text.txt", textData, "utf8");
console.log(promiseFileAppend);
promiseFileAppend.then((err) => {
  console.log(err)
  console.log("Process of writing is done");
})


const promiseReadFile = fs.readFile("text.txt", "utf8");

promiseReadFile
  .then((data) => {
  console.log(data);
  console.log("Process of reading is done");
})
  .catch((err) => {
  console.log(err)
})

// try {
//     fs.readFile("./some.json", "utf8", (err,data) => {
//       if(err) throw err
//       const dataFile = JSON.parse(data);
//       console.log(`Name from objData is: ${dataFile.name}`);
//       console.log("Reading is successfully executed!");
//     } )
// } catch (error) {
//   console.log(error);
// }